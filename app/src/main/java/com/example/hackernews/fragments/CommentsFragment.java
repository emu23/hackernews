package com.example.hackernews.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hackernews.ApiHelper;
import com.example.hackernews.ItemsProvider;
import com.example.hackernews.R;
import com.example.hackernews.models.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "CommentsFragment";
    private static final String ARG_PARENT = "parent";

    private long parent;
    private List<Comment> comments;

    private OnFragmentInteractionListener listener;

    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private SuccessReceiver successReceiver;
    private ErrorReceiver errorReceiver;

    public CommentsFragment() {
        Log.d(TAG, "Constructor");
        // Required empty public constructor
    }

    public static CommentsFragment newInstance(long parent) {
        Log.d(TAG, "newInstance");
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARENT, parent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            parent = args.getLong(ARG_PARENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragment_comments, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        successReceiver = new SuccessReceiver();
        IntentFilter successFilter = new IntentFilter("commentsDownloaded");
        getContext().registerReceiver(successReceiver, successFilter);

        errorReceiver = new ErrorReceiver();
        IntentFilter errorFilter = new IntentFilter("loadStoriesFailed");
        getContext().registerReceiver(errorReceiver, errorFilter);

        if (comments == null) {
            recyclerView.setAdapter(new CommentsAdapter(new ArrayList<Comment>()));
            swipeRefreshLayout.setRefreshing(true);
            onRefresh();
        } else {
            recyclerView.setAdapter(new CommentsAdapter(comments));
        }

        return  view;
    }

    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
        getContext().unregisterReceiver(successReceiver);
        getContext().unregisterReceiver(errorReceiver);
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh");
        ApiHelper.loadComments(getContext(), parent);
    }


    public class SuccessReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "SuccessReceiver.onReceive");
            int commentsPackHash = intent.getExtras().getInt("comments_hash");
            comments = (List<Comment>) ItemsProvider.getItemsPackByHash(commentsPackHash);
            CommentsAdapter adapter = new CommentsAdapter(comments);
            recyclerView.setAdapter(adapter);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public class ErrorReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "ErrorReceiver.onReceive");
            String message = intent.getExtras().getString("message");
            Log.d(TAG, "Error: " + message);
            Snackbar.make(view, "Cannot download commets.", Snackbar.LENGTH_LONG).show();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
        private final String TAG = "CommentsAdapter";

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView author;
            private TextView date;
            private TextView answers;
            private TextView text;

            private Comment comment;

            public ViewHolder(View itemView) {
                super(itemView);
                author = (TextView) itemView.findViewById(R.id.author);
                date = (TextView) itemView.findViewById(R.id.date);
                answers = (TextView) itemView.findViewById(R.id.answers);
                text = (TextView) itemView.findViewById(R.id.text);
                itemView.setOnClickListener(this);
            }

            public void bind(Comment comment) {
                this.comment = comment;
                author.setText(comment.by);
                date.setText(DateUtils.getRelativeTimeSpanString(
                        comment.time * 1000)); // In milliseconds
                int ans = comment.kids.size();
                if (ans > 0) {
                    answers.setText(Integer.toString(ans) + " answers");
                    itemView.setClickable(true);
                } else {
                    answers.setText("");
                    itemView.setClickable(false);
                }
                text.setText(Html.fromHtml(comment.text));
            }

            @Override
            public void onClick(View v) {
                listener.onCommentSelect(comment);
            }
        }

        private List<Comment> comments;

        public CommentsAdapter(List<Comment> comments) {
            this.comments = comments;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.item_comment, parent, false);
            ViewHolder vh = new ViewHolder(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Comment comment = comments.get(position);
            holder.bind(comment);

        }

        @Override
        public int getItemCount() {
            return comments.size();
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        listener = null;
    }

    public interface OnFragmentInteractionListener {
        void onCommentSelect(Comment comment);
    }
}
