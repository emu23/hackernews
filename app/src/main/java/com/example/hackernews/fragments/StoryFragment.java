package com.example.hackernews.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hackernews.R;
import com.example.hackernews.models.Comment;
import com.example.hackernews.models.Story;

import junit.framework.Assert;

public class StoryFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "StoryFragment";
    private static final String ARG_STORY = "story";

    private Story story;

    private OnFragmentInteractionListener listener;

    private View view;
    private TextView score;
    private TextView title;
    private TextView url;
    private FloatingActionButton fab;

    public StoryFragment() {
        Log.d(TAG, "Constructor");
        // Required empty public constructor
    }

    public static StoryFragment newInstance(Story story) {
        Log.d(TAG, "newInstance");
        StoryFragment fragment = new StoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_STORY, story);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            story = (Story) args.getSerializable(ARG_STORY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragment_story, container, false);

        score = (TextView) view.findViewById(R.id.score);
        score.setText(Integer.toString(story.score));
        title = (TextView) view.findViewById(R.id.title);
        title.setText(story.title);
        url = (TextView) view.findViewById(R.id.url);
        if(story.url != null) {
            url.setText(story.url.getHost());
        }

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.comments, CommentsFragment.newInstance(story.id));
        transaction.commit();

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        listener = null;
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick");
        Assert.assertEquals(v, fab);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(story.url.toString()));
        startActivity(intent);
    }

    public void showAnswers(Comment comment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.setCustomAnimations(
                R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.comments, CommentsFragment.newInstance(comment.id));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public interface OnFragmentInteractionListener {

    }
}
