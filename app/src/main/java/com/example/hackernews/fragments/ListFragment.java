package com.example.hackernews.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hackernews.ApiHelper;
import com.example.hackernews.ItemsProvider;
import com.example.hackernews.R;
import com.example.hackernews.async.ItemsSelector;
import com.example.hackernews.models.Story;
import com.example.hackernews.models.ViewType;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "ListFragment";
    private static final String ARG_TYPE = "type";

    private ViewType type;
    private int itemCount = 20;
    private boolean loading = false;
    private List<Story> stories;

    private OnFragmentInteractionListener listener;

    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private StoryAdapter adapter;
    private RecyclerView recyclerView;
    private SuccessReceiver successReceiver;
    private ErrorReceiver errorReceiver;

    public ListFragment() {
        Log.d(TAG, "Constructor");
        // Required empty public constructor
    }

    public static ListFragment newInstance(ViewType type) {
        Log.d(TAG, "newInstance");
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE, type.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            type = ViewType.values()[args.getInt(ARG_TYPE)];
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        view = inflater.inflate(R.layout.fragment_list, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.addOnScrollListener(new OnScrollListener());

        successReceiver = new SuccessReceiver();
        IntentFilter successFilter = new IntentFilter("itemsDownloaded");
        getContext().registerReceiver(successReceiver, successFilter);

        errorReceiver = new ErrorReceiver();
        IntentFilter errorFilter = new IntentFilter("loadItemsFailed");
        getContext().registerReceiver(errorReceiver, errorFilter);

        if (stories == null) {
            adapter = new StoryAdapter(null);
            recyclerView.setAdapter(adapter);
            swipeRefreshLayout.setRefreshing(true);
            onRefresh();
        } else {
            adapter = new StoryAdapter(stories);
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
        getContext().unregisterReceiver(successReceiver);
        getContext().unregisterReceiver(errorReceiver);
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh");
        ItemsSelector is = new ItemsSelector(itemCount, null, null, null, true);
        ApiHelper.loadItems(getContext(), type, is);
    }

    public class OnScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (linearLayoutManager.findLastVisibleItemPosition() == stories.size() && !loading) {
                loading = true;
                itemCount += 20;
                ItemsSelector is = new ItemsSelector(itemCount, null, null, null, true);
                ApiHelper.loadItems(getContext(), type, is);
            }
        }
    }

    public class SuccessReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "SuccessReceiver.onReceive");
            Integer storiesPackHash = intent.getExtras().getInt("items_hash");
            stories = (List<Story>)ItemsProvider.getItemsPackByHash(storiesPackHash);
            adapter.setStories(stories);
            swipeRefreshLayout.setRefreshing(false);
            loading = false;
        }
    }

    public class ErrorReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "ErrorReceiver.onReceive");
            String message = intent.getExtras().getString("message");
            Log.d(TAG, "Error: " + message);
            Snackbar.make(view, "Cannot download stories.", Snackbar.LENGTH_LONG).show();
            swipeRefreshLayout.setRefreshing(false);
            loading = false;
        }
    }

    public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder> {
        private final String TAG = StoryAdapter.class.getSimpleName();
        private final int VIEW_ITEM = 0;
        private final int VIEW_LOADING = 1;

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private View view;
            private int viewType;
            private TextView title;
            private TextView url;
            private TextView score;

            private Story story;

            public ViewHolder(View view, int viewType) {
                super(view);
                this.view = view;
                this.viewType = viewType;
                if (viewType == VIEW_ITEM) {
                    title = (TextView) view.findViewById(R.id.title);
                    url = (TextView) view.findViewById(R.id.url);
                    score = (TextView) view.findViewById(R.id.score);
                    view.setOnClickListener(this);
                }
            }

            public void bind(Story story) {
                Assert.assertEquals(viewType, VIEW_ITEM);
                this.story = story;
                title.setText(story.title);
                if(story.url != null) {
                    url.setText(story.url.getHost());
                }
                score.setText(String.valueOf(story.score));
            }

            @Override
            public void onClick(View v) {
                listener.onStorySelect(story);
            }
        }

        private List<Story> stories;

        public StoryAdapter(List<Story> stories) {
            this.stories = stories;
        }

        @Override
        public int getItemViewType(int position) {
            return position < stories.size() ? VIEW_ITEM : VIEW_LOADING;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            View view;
            if (viewType == VIEW_ITEM)
                view = inflater.inflate(R.layout.item_story, parent, false);
            else
                view = inflater.inflate(R.layout.item_loading, parent, false);

            ViewHolder vh = new ViewHolder(view, viewType);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder vh, int position) {
            if (getItemViewType(position) == VIEW_ITEM) {
                Story story = stories.get(position);
                vh.bind(story);
            }
        }

        @Override
        public int getItemCount() {
            return stories != null ? stories.size() + 1 : 0;
        }

        public void setStories(List<Story> stories) {
            this.stories = stories;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        listener = null;
    }

    public interface OnFragmentInteractionListener {
        void onStorySelect(Story story);
    }
}
