package com.example.hackernews.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hackernews.ItemsProvider;
import com.example.hackernews.models.Comment;
import com.example.hackernews.models.CommentableItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

public class LoadCommentsTask extends AsyncTask<Void, Void, LinkedList<Comment>> {
    private Context context;
    private Long id;

    public LoadCommentsTask(Context activityContext, Long id)
    {
        this.context = activityContext;
        this.id = id;
    }

    @Override
    protected void onPostExecute(LinkedList<Comment> result)
    {
        if(result != null) { // if null then do in background send error intent
            ItemsProvider.addComments(result);
            sendIntent("commentsDownloaded", "comments_hash", result.hashCode());
            Log.i("HN_TASK", "LoadComments: intent sent");
        }
    }

    protected LinkedList<Comment> doInBackground(Void... params) {

        try {
            String itemString = loadItem(id);
            Log.i("HN_TASK", "LoadComments: parent downloaded");
            CommentableItem i = new CommentableItem(new JSONObject(itemString));
            Log.i("HN_TASK", "LoadComments: parent parsed");
            LinkedList<String> commentsStrings = loadComments(i.kids);
            Log.i("HN_TASK", "LoadComments: downloaded comments");
            LinkedList<Comment> comments = parseComments(commentsStrings);
            Log.i("HN_TASK", "LoadComments: parsed comments");
            return comments;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("HN_TASK", "LoadComments: exception handled");
            sendIntent("loadStoriesFailed", "message", e.getMessage());
        }
        return null;
    }

    private String loadItem(Long id) throws Exception {
        String storyPath = "https://hacker-news.firebaseio.com/v0/item/"+String.valueOf(id)+".json";
        return downloadData(storyPath);
    }

    private void sendIntent(String name, String key, Serializable value ) {
        Intent storiesDownloaded = new Intent();
        storiesDownloaded.setAction(name);
        storiesDownloaded.putExtra(key, value);
        context.sendBroadcast(storiesDownloaded);
    }

    private LinkedList<String> loadComments(LinkedList<Long> ids) throws Exception {
        LinkedList<String> stories = new LinkedList<>();
        for(Long id : ids) {
            stories.add(loadItem(id));
            Log.i("HN_TASK", "LoadComments: comment downloaded");
        }
        return stories;
    }

    private LinkedList<Comment> parseComments(LinkedList<String> commentsStrings) throws Exception {
        LinkedList<Comment> comments = new LinkedList<>();
        for(String commentString : commentsStrings) {
            try {
				JSONObject json = new JSONObject(commentString);
				boolean isDeleted = json.optBoolean("deleted", false);
                if(!isDeleted) {
                    comments.add(new Comment(json));
                }
            } catch (JSONException e) {
                throw new Exception("Failed to parse story: "+e.getMessage());
            }
        }
        return comments;
    }

    private String downloadData(String path) throws Exception {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(path);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
                Log.d("Response: ", "> " + line);
            }

            return buffer.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        throw new Exception("Story downloading failed");
    }
}
