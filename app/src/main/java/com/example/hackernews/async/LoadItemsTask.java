package com.example.hackernews.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hackernews.ItemsProvider;
import com.example.hackernews.models.Item;
import com.example.hackernews.models.Job;
import com.example.hackernews.models.Poll;
import com.example.hackernews.models.PollOption;
import com.example.hackernews.models.Story;
import com.example.hackernews.models.ViewType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LoadItemsTask extends AsyncTask<Void, Void, LinkedList<Item>> {
    private Context context;
    private ItemsSelector selector;
    private ViewType type;

    public LoadItemsTask(Context context, ViewType type, ItemsSelector is) {
        this.context = context;
        this.type = type;
        this.selector = is;
    }

    @Override
    protected void onPostExecute(LinkedList<Item> result) {
        if (result != null) { // if null then do in background send error intent
            ItemsProvider.addItems(type, result, true);
            sendIntent("itemsDownloaded", "items_hash", result.hashCode());
            Log.i("HN_TASK", "LoadItems: intent sent");
        }
    }

    protected LinkedList<Item> doInBackground(Void... params) {

        String storiesString = null;
        try {
            storiesString = getItemsIds();
            Log.i("HN_TASK", "LoadItems: ids downloaded");
            LinkedList<Long> ids = parseItemsIds(storiesString);
            Log.i("HN_TASK", "LoadItems: ids parsed");
            LinkedList<JSONObject> itemsJsons = loadItems(ids, selector.count);
            Log.i("HN_TASK", "LoadItems: items downloaded");
            LinkedList<Item> items = parseItems(itemsJsons);
            Log.i("HN_TASK", "LoadItems: items parsed");
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            sendIntent("loadItemsFailed", "message", e.getMessage());
        }
        return null;
    }

    private String getItemsIds() throws Exception {
        String path = GetPath();
        return downloadData(path);
    }

    private String GetPath() {
        String part = "";
        switch (this.type) {
            case ASK_STORIES:
                part = "askstories";
                break;
            case BEST_STORIES:
                part = "beststories";
                break;
            case TOP_STORIES:
                part = "topstories";
                break;
            case JOB_STORIES:
                part = "jobstories";
                break;
            case NEW_STORIES:
                part = "newstories";
                break;
            case SHOW_STORIES:
                part = "showstories";
                break;
            default:
                part = "hotstories";
        }
        return "https://hacker-news.firebaseio.com/v0/" + part + ".json";
    }

    private LinkedList<Long> parseItemsIds(String itemsString) {
        List<String> itemsIds = Arrays.asList(itemsString.substring(1, itemsString.length() - 2).split(","));
        LinkedList<Long> newList = new LinkedList<Long>();
        for (String stringId : itemsIds) {
            newList.add(Long.parseLong(stringId.trim()));
        }
        return newList;
    }

    private LinkedList<JSONObject> loadItems(LinkedList<Long> ids, int count) throws Exception {
        LinkedList<JSONObject> items = new LinkedList<>();
        String itemString = "";
        int properItems = 0;
        for (Long id : ids) {
            if (ShouldUseId(id)) {
                try {
                    itemString = loadItem(id);
                    Log.i("HN_TASK", "LoadStories: item downloaded, id: " + String.valueOf(id));
                } catch (Exception ex) {
                    Log.e("HN_TASK", "Failed to download item with id " + String.valueOf(id) + "message: " + ex.getMessage());
                }
                try {
                    JSONObject item = new JSONObject(itemString);
                    if (IsValidItemType(item)) {
                        items.add(item);
                        properItems++;
                    }
                } catch (JSONException ex) {
                    Log.e("HN_TASK", "Failed to parse item json from string " + itemString);
                }
                if (properItems >= count) {
                    break;
                }
            } else {
                Log.i("HN_TASK", "Skipped story with id: " + String.valueOf(id));
            }
        }
        return items;
    }

    private boolean ShouldUseId(Long id) {
        return
                (selector.biggerThanId == null || id > selector.biggerThanId) &&
                (selector.smallerThanId == null || id < selector.smallerThanId) &&
                (selector.idsToSkip == null || !selector.idsToSkip.contains(id));
    }

    private boolean IsValidItemType(JSONObject item) throws JSONException {
        String itemType = item.getString("type");
        if (itemType.equals("job")) {
            return this.type == ViewType.JOB_STORIES;
        } else {
            return itemType.equals("story"); //right now we don't support polls
        }
    }

    private String loadItem(Long id) throws Exception {
        String storyPath = "https://hacker-news.firebaseio.com/v0/item/" + String.valueOf(id) + ".json";
        return downloadData(storyPath);
    }

    private LinkedList<Item> parseItems(LinkedList<JSONObject> itemsJsons) throws Exception {
        LinkedList<Item> items = new LinkedList<>();
        for (JSONObject itemJson : itemsJsons) {
            try {
                Item item = parseItem(itemJson);
                items.add(item);
            } catch (UnsupportedOperationException e) {
                Log.e("HM_TASK", "failed to parsee item - unknown item type: " + e.getMessage());
            } catch (JSONException e) {
                Log.e("HM_TASK", "Failed to parse item from json: " + itemJson.toString());
            }
        }
        return items;
    }

    private Item parseItem(JSONObject json) throws JSONException, UnsupportedOperationException {
        String itemType = json.getString("type");
        switch (itemType) {
            case "story":
                return new Story(json);
            case "job":
                return new Job(json);
            case "poll":
                return new Poll(json);
            case "pollopt":
                return new PollOption(json);
            default:
                throw new UnsupportedOperationException(itemType);
        }
    }

    private String downloadData(String path) throws Exception {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(path);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
                Log.d("Response: ", "> " + line);
            }

            return buffer.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        throw new Exception("Downloading failed path: " + path);
    }

    private void sendIntent(String name, String key, Serializable value) {
        Intent storiesDownloaded = new Intent();
        storiesDownloaded.setAction(name);
        storiesDownloaded.putExtra(key, value);
        context.sendBroadcast(storiesDownloaded);
    }
}
