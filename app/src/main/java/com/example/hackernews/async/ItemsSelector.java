package com.example.hackernews.async;

import java.util.List;

/**
 * Created by Emil on 2017-06-11.
 */

public class ItemsSelector {
    public int count;
    public List<Long> idsToSkip;
    public Long biggerThanId;
    public Long smallerThanId;
    public boolean addAtEnd;

    public ItemsSelector(int count, List<Long> idsToSkip, Long bigger, Long smaller, boolean addAtEnd) {
        this.count = count;
        this.idsToSkip = idsToSkip;
        this.biggerThanId = bigger;
        this.smallerThanId = smaller;
        this.addAtEnd = addAtEnd;
    }


}

