package com.example.hackernews.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hackernews.models.Comment;
import com.example.hackernews.models.Item;
import com.example.hackernews.models.Story;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LoadItemTask extends AsyncTask<Void, Void, Item> {
    private Context context;
    private Long id;
    private boolean isStory;

    public LoadItemTask(Context activityContext, Long id, boolean isStory)
    {
        this.context = activityContext;
        this.id = id;
        this.isStory = isStory;
    }

    @Override
    protected void onPostExecute(Item result)
    {
        if(result != null) { // if null then do in background send error intent
            if(isStory) {
                sendIntent("storyDownloaded", "story", (Story)result);
            }
            else{
                sendIntent("commentDownloaded", "comment", (Comment)result);
            }
            Log.i("HN_TASK", "LoadItem: intent sent");
        }
    }

    protected Item doInBackground(Void... params) {
        try {
            String itemString = loadItem(id);
            Log.i("HN_TASK", "LoadItem: downloaded item");
            JSONObject json = new JSONObject(itemString);
            Log.i("HN_TASK", "LoadItem: parsed JSON");
            Item result = null;
            if(isStory){
                result =  new Story(new JSONObject(itemString));
            }
            else{
                result = new Comment(new JSONObject(itemString));
            }
            Log.i("HN_TASK", "LoadItem: created object");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("HN_TASK", "LoadItem: exception handled");
            sendIntent("loadItem", "message", e.getMessage());
        }
        return null;
    }

    private void sendIntent(String name, String key, Serializable value ) {
        Intent storiesDownloaded = new Intent();
        storiesDownloaded.setAction(name);
        storiesDownloaded.putExtra(key, value);
        context.sendBroadcast(storiesDownloaded);
    }

    private String loadItem(Long id) throws Exception {
        String storyPath = "https://hacker-news.firebaseio.com/v0/item/"+String.valueOf(id)+".json";
        return downloadData(storyPath);
    }

    private String downloadData(String path) throws Exception {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(path);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
                Log.d("Response: ", "> " + line);
            }

            return buffer.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        throw new Exception("Story downloading failed");
    }
}
