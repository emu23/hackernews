package com.example.hackernews;

import android.content.Context;

import com.example.hackernews.async.ItemsSelector;
import com.example.hackernews.async.LoadCommentsTask;
import com.example.hackernews.async.LoadItemTask;
import com.example.hackernews.async.LoadItemsTask;
import com.example.hackernews.models.ViewType;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Emil on 2017-04-17.
 */

public class ApiHelper {
    private static final String apiPath = "https://hacker-news.firebaseio.com/v0/";

    public static void loadItems(Context context, ViewType type, ItemsSelector is){
        LoadItemsTask task = new LoadItemsTask(context, type, is);
        task.execute();
    }

    public static void loadItem(Context context, long id, boolean isStory){
        LoadItemTask task = new LoadItemTask(context, id, isStory);
        task.execute();
    }

    public static void loadComments(Context context, long id){
        LoadCommentsTask task = new LoadCommentsTask(context, id);
        task.execute();
    }
}

