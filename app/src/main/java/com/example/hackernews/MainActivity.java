package com.example.hackernews;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.example.hackernews.fragments.CommentsFragment;
import com.example.hackernews.fragments.ListFragment;
import com.example.hackernews.fragments.StoryFragment;
import com.example.hackernews.models.Comment;
import com.example.hackernews.models.Story;
import com.example.hackernews.models.ViewType;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        ListFragment.OnFragmentInteractionListener,
        StoryFragment.OnFragmentInteractionListener,
        CommentsFragment.OnFragmentInteractionListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navView;

    private StoryFragment storyFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(this);

        // If activity is recreated from bundle fragment is created automatically
        if (savedInstanceState == null) {
            navView.setCheckedItem(R.id.nav_top_stories);
            setListFragment(ViewType.TOP_STORIES);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (storyFragment != null &&
                   storyFragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
            storyFragment.getChildFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.d(TAG, "onNavigationItemSelected");
        int id = item.getItemId();
        if (id == R.id.nav_top_stories) {
            Log.d(TAG, "Selected top stories.");
            setListFragment(ViewType.TOP_STORIES);
        } else if (id == R.id.nav_best_stories) {
            Log.d(TAG, "Selected best stories.");
            setListFragment(ViewType.BEST_STORIES);
        } else if (id == R.id.nav_new_stories) {
            Log.d(TAG, "Selected new stories.");
            setListFragment(ViewType.NEW_STORIES);
        } else {
            Log.w(TAG, "Selected unknown menu item.");
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setListFragment(ViewType type) {
        Log.d(TAG, "setListFragment");
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content, ListFragment.newInstance(type));
        ft.commit();
    }

    @Override
    public void onStorySelect(Story story) {
        Log.d(TAG, "Story selected: " + story.title);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(
                R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);
        storyFragment = StoryFragment.newInstance(story);
        transaction.replace(R.id.content, storyFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onCommentSelect(Comment comment) {
        Log.d(TAG, "Comment selected: " + comment.id);
        storyFragment.showAnswers(comment);
    }
}
