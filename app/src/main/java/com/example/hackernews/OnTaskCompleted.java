package com.example.hackernews;

import android.content.Context;

public interface OnTaskCompleted{
    void onTaskCompleted();
    Context getActivityContext();
}
