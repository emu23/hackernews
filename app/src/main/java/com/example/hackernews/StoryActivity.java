package com.example.hackernews;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.hackernews.models.Story;

public class StoryActivity extends AppCompatActivity implements OnTaskCompleted{

    private Story story;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        story = new Story();
        setContentView(R.layout.activity_story);
        TextView tv = (TextView)findViewById(R.id.story_title);

    }


    @Override
    public void onTaskCompleted() {
        TextView tv = (TextView)findViewById(R.id.story_title);
        tv.setText(story.title);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
