package com.example.hackernews.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Emil on 2017-05-16.
 */

public class CommentableItem extends Item {
    public LinkedList<Long> kids;

    public CommentableItem(JSONObject json) throws JSONException {
        super(json);
        kids = new LinkedList<Long>();
        if(!json.isNull("kids")) {
            JSONArray array = json.getJSONArray("kids");
            for (int i = 0; i < array.length(); i++) {
                long id = array.optLong(i);
                kids.push(id);
            }
        }
    }

    public CommentableItem(){
        super();
        kids = new LinkedList<Long>();
    }
}
