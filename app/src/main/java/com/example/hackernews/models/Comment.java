package com.example.hackernews.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Emil on 2017-04-22.
 */

public class Comment extends CommentableItem implements Serializable {
    public long parent;
    public String text;

    public Comment() {
        super();
    }

    public Comment(JSONObject json) throws JSONException {
        super(json);
        this.parent = json.getInt("parent");
        this.text = json.getString("text");
    }
}
