package com.example.hackernews.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URI;

/**
 * Created by Emil on 2017-04-17.
 */

public class Story extends CommentableItem implements Serializable {
    public int descendants;
    public int score;
    public String title;
    public URI url;

    public StoryType storyType;

    public Story(){
        super();
    }

    public Story(JSONObject json) throws JSONException {
        super(json);
        this.title = json.getString("title");
        this.descendants = json.getInt("descendants");
        this.score = json.getInt("score");

        this.storyType = this.title.startsWith("Show HN") ?
                StoryType.Show : this.title.startsWith("Ask HN:") ? StoryType.Ask : StoryType.Simple;

        if(this.storyType != StoryType.Ask) {
            this.url = URI.create(json.getString("url"));
        }
    }

    public void updateWith(Story created) {
        this.by = created.by;
        this.descendants = created.descendants;
        this.id = created.id;
        this.score = created.score;
        this.time = created.time;
        this.title = created.title;
        this.url = created.url;
    }
}
