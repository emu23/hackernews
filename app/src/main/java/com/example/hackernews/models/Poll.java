package com.example.hackernews.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by Emil on 2017-04-17.
 */

public class Poll extends CommentableItem implements Serializable {
    public int score;
    public int descendants;
    public String text;
    public String title;
    public LinkedList<Long> parts;

    public Poll(){
        super();
        parts = new LinkedList<>();
    }

    public Poll(JSONObject json) throws JSONException {
        super(json);
        this.descendants = json.getInt("descendants");
        this.text = json.getString("text");
        this.score = json.getInt("score");
        this.title = json.getString("title");

        parts = new LinkedList<Long>();
        if(!json.isNull("parts")) {
            JSONArray array = json.getJSONArray("parts");
            for (int i = 0; i < array.length(); i++) {
                long id = array.optLong(i);
                parts.push(id);
            }
        }
    }
}
