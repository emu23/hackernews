package com.example.hackernews.models;

/**
 * Created by Emil on 2017-05-16.
 */

public enum ViewType {
    TOP_STORIES,
    BEST_STORIES,
    NEW_STORIES,
    ASK_STORIES,
    SHOW_STORIES,
    JOB_STORIES,
}
