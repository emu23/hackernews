package com.example.hackernews.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URI;

/**
 * Created by Emil on 2017-04-17.
 */

public class Job extends Item implements Serializable {
    public int score;
    public String text;
    public String title;
    public URI url;

    public Job(){
        super();
    }

    public Job(JSONObject json) throws JSONException {
        super(json);
        if(!json.isNull("url")) {
            this.url = URI.create(json.getString("url"));
        }
        if(!json.isNull("text")) {
            this.text = json.getString("text");
        }
        this.score = json.getInt("score");
        this.title = json.getString("title");
    }
}
