package com.example.hackernews.models;

/**
 * Created by Emil on 2017-06-05.
 */

enum StoryType {
    Simple,
    Ask,
    Show
}
