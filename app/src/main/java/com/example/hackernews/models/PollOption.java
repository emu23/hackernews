package com.example.hackernews.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Emil on 2017-04-17.
 */

public class PollOption extends Item implements Serializable {
    public int score;
    public long poll; //parent
    public String text;

    public PollOption(){
        super();
    }

    public PollOption(JSONObject json) throws JSONException {
        super(json);
        this.text = json.getString("text");
        this.score = json.getInt("score");
        this.poll = json.getLong("poll");
    }
}
