package com.example.hackernews.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Emil on 2017-04-22.
 */

public class Item implements Serializable {
    public long id;
    public String by;
    public String type;
    public long time;
    public long lastChecked;

    public Item(JSONObject json) throws JSONException {
        this.time = json.getLong("time");
        this.id = json.getLong("id");
        this.type = json.getString("type");
        if(!json.isNull("by")) {
            this.by = json.getString("by");
        }

        this.lastChecked = (new Date()).getTime();
    }

    public Item(){}

    public Date getDate(){
        return new Date((long)time*1000);
    }

    @Override
    // id and time by itself should be enough
    public int hashCode() {
        return (int)(id ^ (id >>> 32)) * 37 +
                (int)(time ^ (time >>> 32)) +
                by == null ? 0 : by.hashCode() +
                type == null ? 0 : type.hashCode();
    }
}
