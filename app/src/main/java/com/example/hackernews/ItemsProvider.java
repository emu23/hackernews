package com.example.hackernews;

import com.example.hackernews.models.Comment;
import com.example.hackernews.models.Item;
import com.example.hackernews.models.ViewType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Emil on 2017-05-28.
 */

public class ItemsProvider {
    public static List<Item> getItemsList(ViewType type){
        switch(type){
            case NEW_STORIES:
                return getNewItems();
            case ASK_STORIES:
                return getAskItems();
            case BEST_STORIES:
                return getBestItems();
            case JOB_STORIES:
                return getJobItems();
            case SHOW_STORIES:
                return getShowItems();
            case TOP_STORIES:
                return getTopItems();
        }

        return null;
    }

    public static List<? extends Item> getItemsPackByHash(Integer hash){
        if(downloadedPacks == null){
            return null;
        }
        Item[] items =  downloadedPacks.get(hash);
        downloadedPacks.remove(hash);
        return new ArrayList<>(Arrays.asList(items));
    }

    public static void addItems(ViewType type, List<Item> newItems, boolean atBegining){
        List<Item> items = getItemsList(type);
        if(atBegining) {
            for(Item i : newItems){
                items.add(0, i);
            }
        }
        else{
            items.addAll(newItems);
        }

        Item[] array = newItems.toArray(new Item[newItems.size()]);
        addDownloadedPack(array, newItems.hashCode());
    }

    public static void addComments(List<Comment> comments){
        Item[] array = comments.toArray(new Item[comments.size()]);
        addDownloadedPack(array, comments.hashCode());
    }

    private static HashMap<Integer, Item[]> downloadedPacks;
    private static void addDownloadedPack(Item[] pack, Integer hash){
        if(downloadedPacks == null){
            downloadedPacks = new HashMap<>();
        }
        downloadedPacks.put(hash, pack);
    }

    // separate fields for all types of feed
    private static List<Item> newItems;
    private static List<Item> getNewItems(){
        if(newItems == null){
            newItems = new ArrayList<>();
        }
        return newItems;
    }

    private static List<Item> askItems;
    private static List<Item> getAskItems(){
        if(askItems == null){
            askItems = new ArrayList<>();
        }
        return askItems;
    }

    private static List<Item> topItems;
    private static List<Item> getTopItems(){
        if(topItems == null){
            topItems = new ArrayList<>();
        }
        return topItems;
    }

    private static List<Item> jobItems;
    private static List<Item> getJobItems(){
        if(jobItems == null){
            jobItems = new ArrayList<>();
        }
        return jobItems;
    }

    private static List<Item> showItems;
    private static List<Item> getShowItems(){
        if(showItems == null){
            showItems = new ArrayList<>();
        }
        return showItems;
    }

    private static List<Item> bestItems;
    private static List<Item> getBestItems(){
        if(bestItems == null){
            bestItems = new ArrayList<>();
        }
        return bestItems;
    }
}
